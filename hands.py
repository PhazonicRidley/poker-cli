from enum import IntEnum
import copy
from colorama import Fore, Style


class HandTypes(IntEnum):
    """Enum of possible hands"""
    HIGH_CARD = 1
    PAIR = 2
    TWO_PAIR = 3
    THREE_OF_A_KIND = 4
    STRAIGHT = 5
    FLUSH = 6
    FULL_HOUSE = 7
    FOUR_OF_A_KIND = 8
    STRAIGHT_FLUSH = 9
    ROYAL_FLUSH = 10

    def __str__(self):
        res = "%s.%s" % (self.__class__.__name__, self._name_)
        return res[10:].replace("_", " ").title()


class HandOfCards:
    """Represents a hand of cards"""
    def __init__(self, cards: list):
        self.cards, self.values = HandOfCards.sort_cards(cards)
        self.current_hand: HandTypes = None
        self.highest_card = self.values[-1]
        self.determine_hand()

    def set_highest_hand(self, hand: HandTypes):
        """Sets the highest hand found"""
        if not self.current_hand or hand > self.current_hand:
            self.current_hand = hand

    def swap_cards(self, deck: list):
        """Swaps cards out in one's hand"""
        card_enumeration = enumerate(self.cards, start=1)
        for n, c in card_enumeration:
            print(f"{n}: {c}")
        inp = input(
            "Which cards would you like to remove, use the position numbers, you may remove up to three cards, separate each card number by a comma.\npoker> ")

        to_remove = inp.split(",")
        if len(to_remove) > 3:
            print("Invalid input, returing to main menu\n")
            return False

        idx_remove = []
        for n in to_remove:
            try:
                idx = int(n) - 1
            except ValueError:
                print("invalid input, start over")
                return False
            idx_remove.append(idx)

        idx_remove.sort(reverse=True)
        for i in idx_remove:
            card = self.cards.pop(i)
            deck.append(card)

        new_cards = []
        for i in range(len(idx_remove)):
            new_cards.append(deck.pop(0))

        self.cards.extend(new_cards)
        self.cards, self.values = HandOfCards.sort_cards(self.cards)
        self.determine_hand()
        print(f"New cards: {self.cards}, current hand: {Fore.RED}{str(self.current_hand)}{Style.RESET_ALL}")
        return True

    def determine_hand(self):
        """Determines the type of hand"""
        suit_lists = {
            "D": [],
            "C": [],
            "H": [],
            "S": []
        }

        for card in self.cards:
            suit_lists[card[0]].append(card[1])

        # finds duplicate cards that may be in a given hand
        duplicates = []
        vals = list(x[1] for x in self.cards)
        for card_val in set(vals):
            if vals.count(card_val) > 1:
                duplicates.append(tuple([vals.count(card_val), card_val]))

        # checks for pairs, three of kind, and four of a kind
        if len(duplicates) == 1:
            if duplicates[0][0] == 2:
                self.set_highest_hand(HandTypes.PAIR)
            elif duplicates[0][0] == 3:
                self.set_highest_hand(HandTypes.THREE_OF_A_KIND)
            elif duplicates[0][0] == 4:
                self.set_highest_hand(HandTypes.FOUR_OF_A_KIND)

        # checks for two pairs or a full house
        if len(duplicates) == 2:
            # print("Tuple of duplicates:", tuple(duplicates.items()))
            if duplicates[0][0] == 2 and duplicates[1][0] == 2:
                self.set_highest_hand(HandTypes.TWO_PAIR)

            elif (duplicates[0][0] == 3 or duplicates[1][0] == 3) and (duplicates[0][0] == 2 or duplicates[1][0] == 2):
                self.set_highest_hand(HandTypes.FULL_HOUSE)

        # if not hand is set, take the high card
        if not self.current_hand:
            self.set_highest_hand(HandTypes.HIGH_CARD)

        # find all things relating to straights, flushes, straight flushes, or royal flushes
        self.straight_flush(suit_lists)

    def check_straight(self) -> tuple:
        """Checks to see if a hand is a straight, will also return a sign if a royal flush configuration is found"""
        incr_by_one_count = 0
        low_ace_checker = []
        if "Ace" in dict(self.cards).values():
            low_ace_checker = copy.deepcopy(self.values)
            low_ace_checker[-1][1] = 1
            low_ace_checker.sort(key=lambda x: x[1])

        for i in range(len(self.values)):
            try:
                if (self.values[i][1] + 1 == self.values[i + 1][1]) or (
                        low_ace_checker[i][1] == low_ace_checker[i + 1][1]):
                    incr_by_one_count += 1
            except IndexError:
                if (self.values[i][1] == self.values[-1][1]) or (
                        low_ace_checker and (low_ace_checker[i][1] == low_ace_checker[-1][1])):
                    incr_by_one_count += 1
                break

        royal_config = False
        if incr_by_one_count == len(self.values):
            self.set_highest_hand(HandTypes.STRAIGHT)
            royal_config = [x[1] for x in self.cards] == [10, "Jack", "Queen", "King", "Ace"]

        return bool(self.current_hand == HandTypes.STRAIGHT), royal_config

    def check_flush(self, suit_sort: dict) -> bool:
        """Checks to see if a flush is found"""
        suit = [k for k, v in suit_sort.items() if v][0]
        if len(suit_sort[suit]) == len(self.cards):
            self.set_highest_hand(HandTypes.FLUSH)
            return True
        else:
            return False

    def straight_flush(self, suit_sort: dict):
        """Checks for a straight or royal flush"""
        s_out = self.check_straight()
        f_out = self.check_flush(suit_sort)
        if s_out[0] and f_out:
            if s_out[1]:
                self.set_highest_hand(HandTypes.ROYAL_FLUSH)
            else:
                self.set_highest_hand(HandTypes.STRAIGHT_FLUSH)

    @staticmethod
    def sort_cards(cards: list) -> tuple:
        """Sorts cards"""

        # considers ace is high until proven otherwise.
        face_cards = {
            "Jack": 11,
            "Queen": 12,
            "King": 13,
            "Ace": 14
        }

        face_cards_values = {
            11: "Jack",
            12: "Queen",
            13: "King",
            14: "Ace"
        }

        # gets raw numeric card values. ie jack = 11.
        numeric_card_values = []
        for c in cards:
            if c[1] in face_cards.keys():
                numeric_card_values.append([c[0], face_cards[c[1]]])
            else:
                numeric_card_values.append(c)

        # sorts cards in acceding order
        numeric_card_values.sort(key=lambda x: x[1])
        cards = []
        for n in numeric_card_values:
            if n[1] in face_cards_values.keys():
                cards.append([n[0], face_cards_values[n[1]]])
            else:
                cards.append(n)

        # returns sorted cards and raw numeric values in a tuple
        return cards, numeric_card_values
