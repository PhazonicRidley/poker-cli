import random
from hands import HandOfCards
from colorama import Fore, Style, init as color_init

# makes deck. list of lists. [suit, card]
deck = []
for suit in ['S', 'H', 'D', 'C']:
    for card in ['Ace', 'King', 'Queen', 'Jack'] + [x for x in range(2, 11)]:
        deck.append([suit, card])

color_init()  # make sure colors work on all terminals

# main game loop
new_turn = True
while True:
    if new_turn:
        # shuffles deck
        random.shuffle(deck)
        player_cards = []
        computer_cards = []
        # deals cards out
        for i in range(1, 11):
            if i % 2 == 0:
                computer_cards.append(deck.pop(0))
            else:
                player_cards.append(deck.pop(0))

        # makes card objects
        player_hand = HandOfCards(player_cards)
        computer_hand = HandOfCards(computer_cards)

    # get user input
    inp = input(f"Your current hand is: {player_hand.cards} Which is a {Fore.RED}{str(player_hand.current_hand)}{Style.RESET_ALL}\nwhat would you like to do? ('q' or 'exit' to quit, 'swap' to swap some cards out if you haven't already, 'show' to finish your turn)\npoker> ")
    if inp.lower() in ("q", "exit"):
        print("Exiting")
        exit(0)

    elif inp.lower() == "swap":
        new_turn = False
        res = player_hand.swap_cards(deck)
        if res:
            inp = "show"
        else:
            continue

    # compares the two hands, player vs the computer
    if inp.lower() == "show":
        print(f"Computer Hand is: {computer_hand.cards}, which is a {Fore.RED}{str(computer_hand.current_hand)}{Style.RESET_ALL}")
        if player_hand.current_hand > computer_hand.current_hand:
            print("You win!")
        elif player_hand.current_hand < computer_hand.current_hand:
            print("You lost!")
        else:
            if player_hand.highest_card[1] > computer_hand.highest_card[1]:
                print("You win!")
            elif player_hand.highest_card[1] < computer_hand.highest_card[1]:
                print("You lost!")
            else:
                print("It is a tie!")

    if not inp.lower() in ('show', 'swap', 'q'):
        print("Invalid option")
        new_turn = False
        continue

    # put the cards back into the deck and start a new turn
    deck.extend(player_hand.cards)
    deck.extend(computer_hand.cards)
    new_turn = True
    print("\n")
